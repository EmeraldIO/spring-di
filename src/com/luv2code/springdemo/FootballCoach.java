package com.luv2code.springdemo;

public class FootballCoach implements Coach {
	private FortuneService fortuneService;
	
	
	
	public FootballCoach(FortuneService fortuneService) {
		this.fortuneService = fortuneService;
	}

	@Override
	public String getDailyWorkout() {
		return "You should run 1km and make 25 push ups.";
	}

	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}
}
